# icepack

Put your dependencies on ice.

## Options

The options object can be in any of the following places, which are merged in order:

| Location |
| -------- |
| `icepack` property of `package.json` |
| `icepack.config.json` |

### Options

| Option | Required | Default | Description |
| ------ | -------- | ------- | ----------- |
| `output` | false | `./ice_modules` | Where the resulting ES module bundles will be placed. |
| `clean` | false | `false` | Whether to empty the `output` directory before creating new bundles. |
| `modules` | true | `[]` | The modules to bundle |
| `map` | false | `{}` | Map some module name to a different output name. Useful to eliminate nested output folders or change extensions like `.mjs` or drop things like `.esm.js`. Only include mappings you'd like to change. The default doesn't modify the output filename |

## Run

`npx @thomasrandolph/icepack`

Alternatively:

```
npm install --save-dev @thomasrandolph/icepack
```

Then, either add a script to your `package.json`:

```
{
	"icepack": "icepack"
}
```

...and run it with `npm run icepack`.

Or, run it directly:

`node ./node_modules/.bin/icepack`