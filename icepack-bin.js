#!/usr/bin/env node

import { readFileSync, existsSync } from "fs";

import { rollup } from "rollup";
import resolve from "@rollup/plugin-node-resolve";
import cjs from "@rollup/plugin-commonjs";
import fsExtra from "fs-extra";

var pkg;
var opts;

var { emptyDirSync } = fsExtra;

function getOutputName( map, entry ){
	var name = entry;

	if( map[ entry ] ){
		name = map[ entry ];
	}

	if( name.endsWith( ".js" ) ){
		name = name.substring( 0, name.length - 3 );
	}

	return name;
}

if( existsSync( "./package.json" ) ){
	pkg = JSON.parse( readFileSync( "./package.json", "utf8" ) );

	pkg = pkg.icepack || {};
}

if( existsSync( "./icepack.config.json" ) ){
	opts = JSON.parse( readFileSync( "./icepack.config.json", "utf8" ) );
}

opts = {
	"output": "./ice_modules",
	"clean": false,
	"map": {},
	...pkg,
	...opts
};

if( opts.clean ){
	emptyDirSync( opts.output );
}

if( opts.modules ){
	rollup( {
		"input": opts.modules.reduce( ( map, entry ) => {
			map[ getOutputName( opts.map, entry ) ] = entry;

			return map;
		}, {} ),
		"plugins": [
			resolve(),
			cjs()
		]
	} ).then( ( memBundle ) => {
		memBundle.write( {
			"format": "es",
			"dir": opts.output,
			"chunkFileNames": "common/[name]-[hash].js"
		} );
	} );
}
